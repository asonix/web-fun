use crate::schema::{FunSchema, Post, PostsByTag, TagSet};
use actix_web::{
    error::ErrorInternalServerError,
    web::{self, Data, Json, Query},
    App, HttpServer,
};
use bonsaidb::{
    client::{url::Url, AsyncClient, AsyncRemoteDatabase},
    core::{
        connection::{AsyncConnection, AsyncStorageConnection},
        schema::SerializedCollection,
    },
    local::config::Builder,
    server::{DefaultPermissions, Server, ServerConfiguration},
};
use rand::seq::SliceRandom;
use schema::{PostInteraction, PostRanksByTime, TagAlias, TagsByAlias};
use std::{collections::HashSet, time::Duration};

mod schema {
    use std::{collections::HashMap, str::Utf8Error};

    use crate::Permute;
    use bonsaidb::core::{
        document::{DocumentId, Emit},
        key::{Key, KeyEncoding},
        schema::{Collection, CollectionViewSchema, Schema, View},
    };
    use serde::{Deserialize, Serialize};
    use time::{format_description::well_known::Rfc3339, OffsetDateTime};

    #[derive(Debug, Schema)]
    #[schema(name = "FunSchema", collections = [Post, TagAlias, PostInteraction])]
    pub struct FunSchema;

    #[derive(Clone, PartialEq)]
    pub struct TagSet {
        tags: Vec<String>,
    }

    impl TagSet {
        pub fn new(mut tags: Vec<String>) -> Self {
            tags.sort();

            Self { tags }
        }
    }

    impl<'k> KeyEncoding<'k> for TagSet {
        const LENGTH: Option<usize> = None;

        type Error = Utf8Error;

        fn as_ord_bytes(&'k self) -> Result<std::borrow::Cow<'k, [u8]>, Self::Error> {
            let total_len = self
                .tags
                .iter()
                .fold(0, |acc, tag| acc + tag.as_bytes().len())
                + self.tags.len();

            let mut out = Vec::with_capacity(total_len);
            for tag in &self.tags {
                out.extend_from_slice(tag.as_bytes());
                out.push(0);
            }

            Ok(std::borrow::Cow::Owned(out))
        }

        fn describe<Visitor>(visitor: &mut Visitor)
        where
            Visitor: bonsaidb::core::key::KeyVisitor,
        {
            visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes);
        }
    }

    impl<'k> Key<'k> for TagSet {
        const CAN_OWN_BYTES: bool = true;

        fn from_ord_bytes<'e>(
            bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
        ) -> Result<Self, Self::Error> {
            let indices = bytes
                .as_ref()
                .iter()
                .enumerate()
                .filter_map(|(index, byte)| if *byte == 0 { Some(index) } else { None })
                .collect::<Vec<_>>();

            if indices.is_empty() {
                match bytes {
                    bonsaidb::core::key::ByteSource::Owned(owned) => {
                        let tag = String::from_utf8(owned).map_err(|e| e.utf8_error())?;
                        return Ok(TagSet { tags: vec![tag] });
                    }
                    bonsaidb::core::key::ByteSource::Ephemeral(ephemeral) => {
                        let tag = std::str::from_utf8(ephemeral)?.into();
                        return Ok(TagSet { tags: vec![tag] });
                    }
                    bonsaidb::core::key::ByteSource::Borrowed(borrowed) => {
                        let tag = std::str::from_utf8(borrowed)?.into();
                        return Ok(TagSet { tags: vec![tag] });
                    }
                }
            }

            let mut tags = Vec::new();
            let mut start = 0;

            for index in indices {
                tags.push(std::str::from_utf8(&bytes.as_ref()[start..index])?.into());
                start = index + 1;
            }

            Ok(TagSet { tags })
        }
    }

    #[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
    pub struct HumanDate(#[serde(with = "time::serde::rfc3339")] OffsetDateTime);

    #[derive(Debug)]
    pub enum HumanDateError {
        Format(time::error::Format),
        Parse(time::error::Parse),
        Bytes(std::str::Utf8Error),
    }

    impl std::fmt::Display for HumanDateError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Self::Format(_) => write!(f, "Error formatting date"),
                _ => write!(f, "Error parsing date"),
            }
        }
    }

    impl std::error::Error for HumanDateError {
        fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
            match self {
                Self::Format(fmt) => Some(fmt),
                Self::Parse(parse) => Some(parse),
                Self::Bytes(bytes) => Some(bytes),
            }
        }
    }

    impl From<time::error::Format> for HumanDateError {
        fn from(value: time::error::Format) -> Self {
            HumanDateError::Format(value)
        }
    }
    impl From<time::error::Parse> for HumanDateError {
        fn from(value: time::error::Parse) -> Self {
            HumanDateError::Parse(value)
        }
    }
    impl From<std::str::Utf8Error> for HumanDateError {
        fn from(value: std::str::Utf8Error) -> Self {
            HumanDateError::Bytes(value)
        }
    }

    impl<'k> KeyEncoding<'k> for HumanDate {
        type Error = HumanDateError;

        const LENGTH: Option<usize> = None;

        fn as_ord_bytes(&'k self) -> Result<std::borrow::Cow<'k, [u8]>, Self::Error> {
            let formatted = self.0.format(&Rfc3339)?;

            Ok(std::borrow::Cow::Owned(Vec::from(formatted)))
        }

        fn describe<Visitor>(visitor: &mut Visitor)
        where
            Visitor: bonsaidb::core::key::KeyVisitor,
        {
            visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
        }
    }

    impl<'k> Key<'k> for HumanDate {
        const CAN_OWN_BYTES: bool = false;

        fn from_ord_bytes<'e>(
            bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
        ) -> Result<Self, Self::Error> {
            let input_str = std::str::from_utf8(bytes.as_ref())?;
            let parsed = OffsetDateTime::parse(input_str, &Rfc3339)?;

            Ok(HumanDate(parsed))
        }
    }

    #[derive(Clone, Debug, Serialize, Deserialize, Collection)]
    #[collection(name = "posts", views = [PostsByTag])]
    pub struct Post {
        title: String,
        body: String,
        tags: Vec<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        published: Option<HumanDate>,
    }

    #[derive(Clone, Debug, Serialize, Deserialize, Collection)]
    #[collection(name = "tag-aliases", primary_key = String, natural_id = |tag_alias: &TagAlias| Some(tag_alias.alias.clone()), views = [TagsByAlias])]
    pub struct TagAlias {
        pub alias: String,
        pub proper: String,
    }

    #[derive(Clone, Debug, Serialize, Deserialize, Collection)]
    #[collection(name = "post-interactions", views = [PostRanksByTime])]
    pub struct PostInteraction {
        post: DocumentId,
        timestamp: HumanDate,
    }

    impl Post {
        pub fn publish(&mut self) {
            if self.published.is_none() {
                self.published = Some(HumanDate(OffsetDateTime::now_utc()));
            }
        }
    }

    impl PostInteraction {
        pub fn new(post: DocumentId) -> Self {
            Self {
                post,
                timestamp: HumanDate(OffsetDateTime::now_utc()),
            }
        }
    }

    #[derive(Debug, Clone, View)]
    #[view(collection = Post, key = TagSet, value = usize, name = "by-tag")]
    pub struct PostsByTag;

    #[derive(Debug, Clone, View)]
    #[view(collection = TagAlias, key = String, value = usize, name = "by-alias")]
    pub struct TagsByAlias;

    #[derive(Debug, Clone, View)]
    #[view(collection = PostInteraction, key = HumanDate, value = HashMap<DocumentId, f64>)]
    pub struct PostRanksByTime;

    impl CollectionViewSchema for PostsByTag {
        type View = Self;

        fn map(
            &self,
            document: bonsaidb::core::document::CollectionDocument<
                <Self::View as View>::Collection,
            >,
        ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
            let mut tags = document.contents.tags.clone();

            tags.sort();
            tags.reverse();

            tags.permute_bounded(5)
                .map(|tags| {
                    let tags = tags.into_iter().cloned().collect();
                    document.header.emit_key_and_value(TagSet { tags }, 1)
                })
                .collect()
        }

        fn reduce(
            &self,
            mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
            _rereduce: bool,
        ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
            Ok(mappings.iter().map(|m| m.value).sum())
        }
    }

    impl CollectionViewSchema for TagsByAlias {
        type View = Self;

        fn map(
            &self,
            document: bonsaidb::core::document::CollectionDocument<
                <Self::View as View>::Collection,
            >,
        ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
            document
                .header
                .emit_key_and_value(document.contents.alias, 1)
        }

        fn reduce(
            &self,
            mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
            _rereduce: bool,
        ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
            Ok(mappings.iter().map(|m| m.value).sum())
        }
    }

    // The Algorithm (spooky)
    fn the_algorithm(now: HumanDate, time: HumanDate, score: f64) -> f64 {
        let duration = now.0 - time.0;
        let x = duration.whole_seconds() as f64 / 60_f64;

        let y = 1_f64 - (x.sqrt() * (1_f64 / 60_f64.sqrt()));

        if y < 0_f64 {
            return 0_f64;
        }

        y * score
    }

    impl CollectionViewSchema for PostRanksByTime {
        type View = Self;

        fn map(
            &self,
            document: bonsaidb::core::document::CollectionDocument<
                <Self::View as View>::Collection,
            >,
        ) -> bonsaidb::core::schema::ViewMapResult<Self::View> {
            let mut hm = HashMap::new();
            hm.insert(document.contents.post, 1.0);

            document
                .header
                .emit_key_and_value(document.contents.timestamp, hm)
        }

        fn reduce(
            &self,
            mappings: &[bonsaidb::core::schema::ViewMappedValue<Self::View>],
            _rereduce: bool,
        ) -> bonsaidb::core::schema::ReduceResult<Self::View> {
            let now = mappings.iter().map(|mapping| mapping.key).max();

            if let Some(now) = now {
                Ok(mappings.iter().fold(HashMap::new(), |mut acc, mapping| {
                    for (document, score) in &mapping.value {
                        let updated = the_algorithm(now, mapping.key, *score);

                        if updated > 0_f64 {
                            let entry = acc.entry(document.clone()).or_default();

                            *entry += updated;
                        }
                    }
                    acc
                }))
            } else {
                Ok(HashMap::new())
            }
        }
    }
}

async fn index() -> Result<String, actix_web::Error> {
    Ok(format!("hi"))
}

async fn create_post(post: Json<Post>, repo: Data<Repo>) -> Result<String, actix_web::Error> {
    let mut post = post.into_inner();
    post.publish();
    post.push_into_async(&repo.db)
        .await
        .map_err(ErrorInternalServerError)?;

    Ok(String::from("created"))
}

async fn get_posts(
    query: Query<Vec<(String, String)>>,
    repo: Data<Repo>,
) -> Result<Json<Vec<Post>>, actix_web::Error> {
    let posts = if !query.0.is_empty() {
        let mut tags = query
            .into_inner()
            .into_iter()
            .filter_map(|(key, value)| if key == "tag[]" { Some(value) } else { None })
            .collect::<HashSet<_>>();

        let aliases = repo
            .db
            .view::<TagsByAlias>()
            .with_keys(&tags)
            .query_with_collection_docs()
            .await
            .map_err(ErrorInternalServerError)?;

        for alias in aliases.documents.into_values() {
            tags.remove(&alias.contents.alias);
            tags.insert(alias.contents.proper);
        }

        repo.db
            .view::<PostsByTag>()
            .with_key(&TagSet::new(tags.into_iter().collect()))
            .limit(50)
            .query_with_collection_docs()
            .await
            .map_err(ErrorInternalServerError)?
            .documents
            .into_values()
            .map(|post| post.contents)
            .collect()
    } else {
        repo.db
            .collection::<Post>()
            .all()
            .descending()
            .limit(50)
            .await
            .map_err(ErrorInternalServerError)?
            .into_iter()
            .map(|post| Post::document_contents(&post))
            .collect::<Result<Vec<_>, _>>()
            .map_err(ErrorInternalServerError)?
    };

    Ok(Json(posts))
}

async fn create_tag_alias(
    Json(tag_alias): Json<TagAlias>,
    repo: Data<Repo>,
) -> Result<String, actix_web::Error> {
    tag_alias
        .push_into_async(&repo.db)
        .await
        .map_err(ErrorInternalServerError)?;

    Ok(String::from("Inserted"))
}

async fn interact_post(repo: Data<Repo>) -> Result<String, actix_web::Error> {
    let post_ids = repo
        .db
        .collection::<Post>()
        .all()
        .descending()
        .limit(50)
        .await
        .map_err(ErrorInternalServerError)?
        .into_iter()
        .map(|post| post.header.id)
        .collect::<Vec<_>>();

    if let Some(id) = post_ids.choose(&mut rand::thread_rng()) {
        PostInteraction::new(id.clone())
            .push_into_async(&repo.db)
            .await
            .map_err(ErrorInternalServerError)?;
    }

    Ok(String::from("Interacted"))
}

async fn post_ranks(repo: Data<Repo>) -> Result<String, actix_web::Error> {
    let reduction = repo
        .db
        .view::<PostRanksByTime>()
        .reduce()
        .await
        .map_err(ErrorInternalServerError)?;

    Ok(format!("{reduction:?}"))
}

struct Repo {
    db: AsyncRemoteDatabase,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let server = Server::open(
        ServerConfiguration::new_with_backend("./data.bonsaidb", bonsaidb::server::NoBackend)
            .default_permissions(DefaultPermissions::AllowAll)
            .with_schema::<FunSchema>()?,
    )
    .await?;

    if server.certificate_chain().await.is_err() {
        server.install_self_signed_certificate(true).await?;
    }
    let certificate = server
        .certificate_chain()
        .await?
        .into_end_entity_certificate();
    server.create_database::<FunSchema>("fun-db", true).await?;

    let task_server = server.clone();
    let server_task = tokio::spawn(async move { task_server.listen_on(5645).await });

    tokio::time::sleep(Duration::from_millis(200)).await;

    let client = AsyncClient::build(Url::parse("bonsaidb://localhost:5645")?)
        .with_certificate(certificate)
        .build()?;

    let db = client.database::<FunSchema>("fun-db").await?;

    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(Repo { db: db.clone() }))
            .route("/", web::get().to(index))
            .service(
                web::scope("/posts")
                    .route("", web::post().to(create_post))
                    .route("", web::get().to(get_posts))
                    .service(web::resource("/interact").route(web::post().to(interact_post)))
                    .service(web::resource("/ranks").route(web::get().to(post_ranks))),
            )
            .service(web::resource("tags").route(web::post().to(create_tag_alias)))
    })
    .bind("0.0.0.0:8005")?
    .run()
    .await?;

    server_task.abort();
    let _ = server_task.await;

    Ok(())
}

struct Permuter<'a, T> {
    bound: Option<usize>,
    slice: &'a [T],
    inner: Option<(&'a T, Box<Permuter<'a, T>>)>,
}

impl<'a, T> Permuter<'a, T> {
    fn new(slice: &'a [T], bound: Option<usize>) -> Self {
        Self {
            bound,
            slice,
            inner: None,
        }
    }
}

impl<'a, T> Iterator for Permuter<'a, T> {
    type Item = Vec<&'a T>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some((current, inner)) = &mut self.inner {
            if let Some(mut vec) = inner.next() {
                vec.push(current);
                Some(vec)
            } else if self.slice.is_empty() {
                None
            } else {
                self.inner = None;
                self.next()
            }
        } else if self.slice.is_empty() {
            None
        } else {
            let (first, rest) = self.slice.split_at(1);
            self.slice = rest;
            self.inner = if let Some(bound) = self.bound {
                if bound > 0 {
                    Some((&first[0], Box::new(Permuter::new(rest, Some(bound - 1)))))
                } else {
                    return None;
                }
            } else {
                Some((&first[0], Box::new(Permuter::new(rest, None))))
            };

            Some(vec![&first[0]])
        }
    }
}

trait Permute<T> {
    fn permute(&self) -> Permuter<'_, T>;

    fn permute_bounded(&self, bound: usize) -> Permuter<'_, T>;
}

impl<T> Permute<T> for [T] {
    fn permute(&self) -> Permuter<'_, T> {
        Permuter::new(self, None)
    }

    fn permute_bounded(&self, bound: usize) -> Permuter<'_, T> {
        Permuter::new(self, Some(bound))
    }
}

#[cfg(test)]
mod tests {
    use crate::Permute;

    #[test]
    fn permutes_unbounded() {
        let permutations: Vec<_> = [1, 2, 3, 4].permute().collect();

        assert_eq!(
            permutations,
            vec![
                vec![&1],
                vec![&2, &1],
                vec![&3, &2, &1],
                vec![&4, &3, &2, &1],
                vec![&4, &2, &1],
                vec![&3, &1],
                vec![&4, &3, &1],
                vec![&4, &1],
                vec![&2],
                vec![&3, &2],
                vec![&4, &3, &2],
                vec![&4, &2],
                vec![&3],
                vec![&4, &3],
                vec![&4],
            ]
        );
    }

    #[test]
    fn permutes_bounded() {
        let permutations: Vec<_> = [1, 2, 3, 4].permute_bounded(3).collect();

        assert_eq!(
            permutations,
            vec![
                vec![&1],
                vec![&2, &1],
                vec![&3, &2, &1],
                vec![&4, &2, &1],
                vec![&3, &1],
                vec![&4, &3, &1],
                vec![&4, &1],
                vec![&2],
                vec![&3, &2],
                vec![&4, &3, &2],
                vec![&4, &2],
                vec![&3],
                vec![&4, &3],
                vec![&4],
            ]
        );
    }

    #[test]
    fn permutes_bounded_fewer() {
        let permutations: Vec<_> = [1, 2, 3, 4].permute_bounded(2).collect();

        assert_eq!(
            permutations,
            vec![
                vec![&1],
                vec![&2, &1],
                vec![&3, &1],
                vec![&4, &1],
                vec![&2],
                vec![&3, &2],
                vec![&4, &2],
                vec![&3],
                vec![&4, &3],
                vec![&4],
            ]
        );
    }

    #[test]
    fn permutes_bounded_larger() {
        let permutations: Vec<_> = [1, 2, 3, 4, 5].permute_bounded(2).collect();

        assert_eq!(
            permutations,
            vec![
                vec![&1],
                vec![&2, &1],
                vec![&3, &1],
                vec![&4, &1],
                vec![&5, &1],
                vec![&2],
                vec![&3, &2],
                vec![&4, &2],
                vec![&5, &2],
                vec![&3],
                vec![&4, &3],
                vec![&5, &3],
                vec![&4],
                vec![&5, &4],
                vec![&5],
            ]
        );
    }
}
